﻿using System;
using System.Collections.Generic;
using System.Text;
using BLL.Interfaces;
using DAL.Repositories;

namespace BLL.Services
{
    public class ServiceCreator : IServiceCreator
    {
        public IUserService CreateUserService(string connection)
        {
            return new UserService(new AppUnitOfWork(connection));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Threading.Tasks;
using DAL.Identity;

namespace DAL.Repositories
{
    public class AppUnitOfWork : IUnitOfWork
    {
        private  MyAppContext db;

        private AppUserManager userManager;
        private AppRoleManager roleManager;
        private IClientManager clientManager;

        public AppUnitOfWork(string connectionString)
        {
            db = new MyAppContext(connectionString);
            userManager = new AppUserManager(new UserStore<AppUser>(db));
            roleManager = new AppRoleManager(new RoleStore<AppRole>(db));
            clientManager = new ClientManager(db);
        }

        public AppUserManager UserManager
        {
            get { return userManager; }
        }

        public IClientManager ClientManager
        {
            get { return clientManager; }
        }

        public AppRoleManager RoleManager
        {
            get { return roleManager; }
        }

        public async Task SaveAsync()
        {
            await db.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    userManager.Dispose();
                    roleManager.Dispose();
                    clientManager.Dispose();
                }
                this.disposed = true;
            }
        }
    }
}

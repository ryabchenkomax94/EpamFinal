﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using DAL.Entities;

namespace DAL.EF
{
    public class MyAppContext : IdentityDbContext<AppUser>
    {
        public MyAppContext(string conectionString) : base(conectionString) { }

        public DbSet<ClientProfile> ClientProfiles { get; set; }
    }
}

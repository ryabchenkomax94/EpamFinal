﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Entities;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DAL.Entities
{
    public class AppUser : IdentityUser
    {
        public virtual ClientProfile ClientProfile { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

using DAL.Identity;

using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        AppUserManager UserManager { get; }
        IClientManager ClientManager { get; }
        AppRoleManager RoleManager { get; }
        Task SaveAsync();
    }
}
